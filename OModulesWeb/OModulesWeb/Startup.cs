﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OModulesWeb.Startup))]
namespace OModulesWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            //ConfigureAuth(app);
            //LoadModules();
        }

    }
}
